/**
 * MapStruct mappers for mapping domain objects and Data Transfer Objects.
 */
package io.github.enevo.application.service.mapper;
