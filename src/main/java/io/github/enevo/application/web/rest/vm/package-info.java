/**
 * View Models used by Spring MVC REST controllers.
 */
package io.github.enevo.application.web.rest.vm;
