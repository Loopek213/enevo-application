/**
 * Data Access Objects used by WebSocket services.
 */
package io.github.enevo.application.web.websocket.dto;
