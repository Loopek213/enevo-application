import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { EnEvoApplicationSharedCommonModule, JhiLoginModalComponent, HasAnyAuthorityDirective } from './';

@NgModule({
  imports: [EnEvoApplicationSharedCommonModule],
  declarations: [JhiLoginModalComponent, HasAnyAuthorityDirective],
  entryComponents: [JhiLoginModalComponent],
  exports: [EnEvoApplicationSharedCommonModule, JhiLoginModalComponent, HasAnyAuthorityDirective],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class EnEvoApplicationSharedModule {
  static forRoot() {
    return {
      ngModule: EnEvoApplicationSharedModule
    };
  }
}
