import { NgModule } from '@angular/core';

import { EnEvoApplicationSharedLibsModule, JhiAlertComponent, JhiAlertErrorComponent } from './';

@NgModule({
  imports: [EnEvoApplicationSharedLibsModule],
  declarations: [JhiAlertComponent, JhiAlertErrorComponent],
  exports: [EnEvoApplicationSharedLibsModule, JhiAlertComponent, JhiAlertErrorComponent]
})
export class EnEvoApplicationSharedCommonModule {}
